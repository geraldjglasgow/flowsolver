from numpy.core.numeric import Infinity
from BoardHelper import *
from Objects import *

def findBreakablePath(leaf):
    pathToRemove = None
    if isLeftBreakable(leaf):
        pathToRemove = Point(leaf.point.x-1, leaf.point.y)
    if isRightBreakable(leaf):
        pathToRemove = Point(leaf.point.x+1, leaf.point.y)
    if isUpBreakable(leaf):
        pathToRemove = Point(leaf.point.x, leaf.point.y-1)
    if isDownBreakable(leaf):
        pathToRemove = Point(leaf.point.x, leaf.point.y+1)
    return pathToRemove

def findPaths(leaf, endingPoint):
    pathsFound = False
    color = getTile(endingPoint.x, endingPoint.y)
    if isLeft(leaf, color):
        leaf.insertChild(Point(leaf.point.x-1, leaf.point.y), findCost(Point(leaf.point.x-1, leaf.point.y), endingPoint))
        pathsFound = True
    if isRight(leaf, color):
        leaf.insertChild(Point(leaf.point.x+1, leaf.point.y), findCost(Point(leaf.point.x+1, leaf.point.y), endingPoint))
        pathsFound = True
    if isUp(leaf, color):
        leaf.insertChild(Point(leaf.point.x, leaf.point.y-1), findCost(Point(leaf.point.x, leaf.point.y-1), endingPoint))
        pathsFound = True
    if isDown(leaf, color):
        leaf.insertChild(Point(leaf.point.x, leaf.point.y+1), findCost(Point(leaf.point.x, leaf.point.y+1), endingPoint))
        pathsFound = True
    return pathsFound

def isPathBlocked(leaf, color):
    pathBlocked = True
    if isLeft(leaf, color):
        pathBlocked = False
    if isRight(leaf, color):
        pathBlocked = False
    if isUp(leaf, color):
        pathBlocked = False
    if isDown(leaf, color):
        pathBlocked = False
    return pathBlocked

def getLeafNodes(node, leafs):
    if len(node.children) == 0:
        leafs.append(node)

    for child in node.children:
        getLeafNodes(child, leafs)
    return leafs


def findLowestCostUnblockedLeaf(leafs, color):
    lowestCostLeaf = None
    for leaf in leafs:
        if (leaf.cost < (lowestCostLeaf.cost if lowestCostLeaf != None else Infinity)) and not isPathBlocked(leaf, color):
            lowestCostLeaf = leaf
    return lowestCostLeaf

def findLowestCostLeaf(leafs):
    lowestCostLeaf = None
    for leaf in leafs:
        if (leaf.cost < (lowestCostLeaf.cost if lowestCostLeaf != None else Infinity)):
            lowestCostLeaf = leaf
    return lowestCostLeaf


def findLowestCostLeafs(leafs, color):
    lowestCostLeaf = findLowestCostUnblockedLeaf(leafs, color)
    lowestCostLeafs = []
    if lowestCostLeaf != None:
        for leaf in leafs:
            if leaf.cost == lowestCostLeaf.cost:
                lowestCostLeafs.append(leaf)
    return lowestCostLeafs
    