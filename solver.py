from sys import path
import numpy as np
from enum import Enum
from Objects import *
from BoardHelper import *
from DotFunctions import * 
from TreeFunctions import *

from timeit import default_timer as timer
from numpy.core.defchararray import index
from numpy.core.numeric import Infinity


def setup():
    for x in range(len(BOARD)):
        for y in range(len(BOARD)):
            if getTile(x, y) > 0:
                DOTS.append(Dot(Point(x, y), getTile(x, y)))
                COLORS.add(getTile(x, y))


def findStartingNode(indexOne, indexTwo):
    DOTS[indexOne].rootNode = Node(DOTS[indexOne].point, None, None)
    findPaths(DOTS[indexOne].rootNode, DOTS[indexTwo].point)
    
    DOTS[indexTwo].rootNode = Node(DOTS[indexTwo].point, None, None)
    findPaths(DOTS[indexTwo].rootNode, DOTS[indexOne].point)

    if len(DOTS[indexTwo].rootNode.children) < len(DOTS[indexOne].rootNode.children):
        return indexTwo
    return indexOne

def findDotIndexsByColor(color):
    dots = []
    for index, dot in enumerate(DOTS):
        if dot.color == color:
            dots.append(index)
    return dots

def saveCompletedPath(startingIndex):
    parentFound = False
    leafs = getLeafNodes(DOTS[startingIndex].rootNode, [])
    lowestCostLeaf = findLowestCostLeaf(leafs)
    while not parentFound:
        
        DOTS[startingIndex].completePath.append(lowestCostLeaf.point)
        if lowestCostLeaf.parent != None:
            lowestCostLeaf = lowestCostLeaf.parent
        else:
            parentFound = True

def isPathFound(lowestCostLeaf, endingNode):
    return lowestCostLeaf.point.x == endingNode.point.x and lowestCostLeaf.point.y == endingNode.point.y

def removePathByColor(color):
    for x in range(len(BOARD)):
        for y in range(len(BOARD)):
            if getTile(x, y) == color:
                setTile(x, y, 0)

def paintDotsByColor(color):
    dotIndexes = findDotIndexsByColor(color)
    for dotIndex in dotIndexes:
        setTile(DOTS[dotIndex].point.x, DOTS[dotIndex].point.y, color)

def paintPathByColor(color):
    dotIndexes = findDotIndexsByColor(color)
    for dotIndex in dotIndexes:
        points = DOTS[dotIndex].completePath
        for point in points:
            setTile(point.x, point.y, color)

def isAllPathsComplete():
    pathsComplete = True
    for color in COLORS:
        if not isPathComplete(color):
            pathsComplete = False
    return pathsComplete

def isPathComplete(color):
    pathComplete = False
    dotIndexes = findDotIndexsByColor(color)
    for dotIndex in dotIndexes:
        if len(DOTS[dotIndex].completePath) != 0:
            pathComplete = True
    return pathComplete
    


def resetPathsByColor(color):
    dotIndexes = findDotIndexsByColor(color)
    for dotIndex in dotIndexes:
        DOTS[dotIndex].rootNode = None
        DOTS[dotIndex].completePath = []

def createPath(color):
    dotIndexes = findDotIndexsByColor(color)
    startingIndex = findStartingNode(dotIndexes[0], dotIndexes[1])
    endingIndex = None
    if dotIndexes[0] == startingIndex:
        endingIndex = dotIndexes[1]
    else:
        endingIndex = dotIndexes[0]
    startingNode = DOTS[startingIndex].rootNode
    endingNode = DOTS[endingIndex].rootNode


    pathFound = False
    while not pathFound:
        leafs = getLeafNodes(startingNode, [])
        lowestCostLeafs = findLowestCostLeafs(leafs, color)
        pathsFound = False
        for leaf in lowestCostLeafs:
            if findPaths(leaf, endingNode.point):
                pathsFound = True

        if not pathsFound:
            # find first path to break
            leaf = lowestCostLeafs[0] if len(lowestCostLeafs) > 0 else leafs[0]
            pathToRemove = findBreakablePath(leaf)
            colorToRemove = getTile(pathToRemove.x, pathToRemove.y)
            removePathByColor(colorToRemove)
            paintDotsByColor(colorToRemove)
            resetPathsByColor(colorToRemove)
            
        lowestCostLeaf = findLowestCostLeaf(getLeafNodes(startingNode, []))
        pathFound = isPathFound(lowestCostLeaf, endingNode)
        if pathFound:
            saveCompletedPath(startingIndex)
        

start = timer()
setup()
print(np.matrix(BOARD))

while not isAllPathsComplete():
    for color in COLORS:
        if  not isPathComplete(color):
            createPath(color)
            paintPathByColor(color)
            print("\n")   
            print(np.matrix(BOARD))

end = timer()
print(end - start)