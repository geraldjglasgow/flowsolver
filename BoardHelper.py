# Red = 1, Orange = 2, Yellow = 3, Green = 4, Blue = 5, cyan = 6
DOTS = []
COLORS = set()
BOARD = [[0,0,0,0,0,0], [1,2,6,4,0,1], [0,0,0,6,0,4], [0,0,0,0,0,0], [0,0,5,0,0,3], [2,0,0,3,0,5]]
#BOARD = [[0,0,0,0,0,0], [1,5,0,4,0,0], [0,3,0,0,0,0], [0,2,0,0,0,1], [0,3,0,0,4,5], [2,0,0,0,0,0]]

def isLeft(leaf, color):
    return leaf.point.x-1 >= 0 \
        and leaf.point.x-1 <=5 \
        and not isParentTile(leaf.parent, leaf.point.x-1, leaf.point.y) \
        and (getTile(leaf.point.x-1, leaf.point.y) == 0 or getTile(leaf.point.x-1, leaf.point.y) == color)

def isRight(leaf, color):
    return leaf.point.x+1 >= 0 \
        and leaf.point.x+1 <=5 \
        and not isParentTile(leaf.parent, leaf.point.x+1, leaf.point.y) \
        and (getTile(leaf.point.x+1, leaf.point.y) == 0 \
        or getTile(leaf.point.x+1, leaf.point.y) == color)

def isUp(leaf, color):
    return leaf.point.y-1 >= 0 \
        and leaf.point.y-1 <=5 \
        and not isParentTile(leaf.parent, leaf.point.x, leaf.point.y-1) \
        and (getTile(leaf.point.x, leaf.point.y-1) == 0 or getTile(leaf.point.x, leaf.point.y-1) == color)

def isDown(leaf, color):
    return leaf.point.y+1 >= 0 \
        and leaf.point.y+1 <=5 \
        and not isParentTile(leaf.parent, leaf.point.x, leaf.point.y+1) \
        and (getTile(leaf.point.x, leaf.point.y+1) == 0 or getTile(leaf.point.x, leaf.point.y+1) == color)

def isLeftBreakable(leaf):
    return leaf.point.x-1 >= 0 \
        and leaf.point.x-1 <=5 \
        and not isParentTile(leaf.parent, leaf.point.x-1, leaf.point.y) \
        and getTile(leaf.point.x-1, leaf.point.y) != 0

def isRightBreakable(leaf):
    return leaf.point.x+1 >= 0 \
        and leaf.point.x+1 <=5 \
        and not isParentTile(leaf.parent, leaf.point.x+1, leaf.point.y) \
        and getTile(leaf.point.x+1, leaf.point.y) != 0

def isUpBreakable(leaf):
    return leaf.point.y-1 >= 0 \
        and leaf.point.y-1 <=5 \
        and not isParentTile(leaf.parent, leaf.point.x, leaf.point.y-1) \
        and getTile(leaf.point.x, leaf.point.y-1) != 0

def isDownBreakable(leaf):
    return leaf.point.y+1 >= 0 \
        and leaf.point.y+1 <=5 \
        and not isParentTile(leaf.parent, leaf.point.x, leaf.point.y+1) \
        and getTile(leaf.point.x, leaf.point.y+1) != 0


def findCost(currentPoint, endingPoint):
    return abs(currentPoint.x - endingPoint.x) + abs(currentPoint.y - endingPoint.y)

def isParentTile(parent, x, y):
    return parent != None and x == parent.point.x and y == parent.point.y

def getTile(x, y):
    return BOARD[len(BOARD)-y-1][x]

def setTile(x, y, value):
    BOARD[len(BOARD)-y-1][x] = value