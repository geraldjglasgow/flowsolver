from enum import Enum

class Node:
    def __init__(self, point, cost, parent):
        self.children = []
        self.point = point
        self.cost = cost
        self.parent = parent


    def insertChild(self, point, cost):
        # Compare the new value with the parent node    
        self.children.append(Node(point, cost, self))


class Type(Enum):
    EMPTY = 0
    DOT = 1
    PATH = 2

class Tile:
    def __init__(self, x, y, type, color):
        self.x = x
        self.y = y
        self.type = type
        self.color = color

class Dot: 
    rootNode = None
    conflictingPaths = []
    def __init__(self, point, color):
        self.point = point
        self.color = color
        self.completePath = []


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y